"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MyPackageModule = void 0;
const common_1 = require("@nestjs/common");
const mypackage_service_1 = require("./mypackage.service");
let MyPackageModule = class MyPackageModule {
};
MyPackageModule = __decorate([
    common_1.Module({
        providers: [mypackage_service_1.MyPackageService],
        exports: [mypackage_service_1.MyPackageService],
    })
], MyPackageModule);
exports.MyPackageModule = MyPackageModule;
//# sourceMappingURL=mypackage.module.js.map